package pt.ipp.estg.formulario;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import java.util.Collections;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Integer[] ages = new Integer[90];
    private EditText name, description;
    private RadioButton btnM, btnF;
    private Spinner age;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for(int i = 0; i < ages.length; i++){
            ages[i] = i + 1;
        }


        Button btnInfo = findViewById(R.id.btn_info);
        ImageView image = findViewById(R.id.img);

        name = findViewById(R.id.name);
        description = findViewById(R.id.edit_des);

        btnM = findViewById(R.id.male);
        btnF = findViewById(R.id.fem);

        age = findViewById(R.id.spinner_age);

        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(
                this,
                android.R.layout.simple_list_item_1,
                ages
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        age.setAdapter(adapter);

        btnInfo.setOnClickListener(this);
        image.setOnClickListener(this);

        }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent;
        switch(id){
            case R.id.btn_info:
                String gender = null;
                if (btnF.isChecked()){
                    gender = "Feminino";
                }
                else if (btnM.isChecked()){
                    gender = "Masculino";
                }
                intent = new Intent(this, InfoActivity.class);
                intent.putExtra(InfoActivity.NAME, name.getText().toString());
                intent.putExtra(InfoActivity.AGE, age.getSelectedItem().toString());
                intent.putExtra(InfoActivity.GENDER, gender);
                intent.putExtra(InfoActivity.DESC, description.getText().toString());
                startActivity(intent);
                break;
            case R.id.img:
                intent = new Intent(this, ImageShow.class);
                //intent.putExtra(ImageShow.IMG, );
                startActivity(intent);
                break;

        }
    }
}
