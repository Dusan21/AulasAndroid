package pt.ipp.estg.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.Objects;

public class SecondaryActivity extends AppCompatActivity {

    static final String EXTRA = "Extra";
    private static final String NAME = "Secondary Activity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

        String extra = Objects.requireNonNull(getIntent().getExtras()).getString(EXTRA);

        TextView textView = findViewById(R.id.set_text);
        textView.setText(extra);

        Log.d(NAME, "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(NAME, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(NAME, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(NAME, "onPause");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(NAME, "onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(NAME, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(NAME, "onDestroy");
    }
}
