package pt.ipp.estg.layouts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TableActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnFechar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);

        btnFechar = findViewById(R.id.btnFechar);
        btnFechar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnFechar){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}
