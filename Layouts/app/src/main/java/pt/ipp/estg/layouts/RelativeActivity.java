package pt.ipp.estg.layouts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RelativeActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative);

        btnTable = findViewById(R.id.btnTable);
        btnTable.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnTable){
            Intent intent = new Intent(this, TableActivity.class);
            startActivity(intent);
        }
    }
}
